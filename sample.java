public class CallingMethodsInSameClass
{
	public static void main(String[] args) {
		printOne();
		printOne();
		printTwo();
	}
	public static void printOne() {
		System.out.println("Hello World");
	}
	public static int factorial(int n)
	{	int result = 1;
		for(int i = 2; i <= n; i++)
			result *= i;
		return result;
	}
	public String concat(String string1, String string2) throws MyException {
        if(string1 == null) {
        throw new MyException("string1 was null");
        }
        if(string2 == null) {
        throw new MyException("string2 was null");
        }
    return string1 + string2;
}
}